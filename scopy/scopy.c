/*
 * Name: Daniel Brown
 * Class: CS426
 * Professor: Paul Bonamy
 * Assignment: Project Phase 6
 * GIT Repository: https://gitlab.com/Rexious13/cs426
 * Description:
 *  By adding the executable "scopy" in a person's public folder and giving it u+s
 *  permissions, they can share files with another person by creating a config file
 *  for that user specifying who can access it.
 *  The README that specifies how this is done, with examples, can be found at the
 *  specified GIT repository.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pwd.h>

int checkConfigFile(FILE*, struct passwd*);

int main (int argc, char **argv)
{
    struct stat statBuffer;
    int statCheck;
    char inputFile[500], outputFile[500], configFile[507], ch;
    FILE *fp, *input, *output;
    struct passwd *p;

    if (argc != 3) {
        printf("Must specify a source file and a target file!\n");
        exit(EXIT_FAILURE);
    }

    // Copy the user's UID and the program's UID.
    uid_t ruid = getuid();
    uid_t euid = geteuid();

    // Copy the input, output, and config files.
    strncpy(inputFile, argv[1], 500);
    strncpy(outputFile, argv[2], 500);
    strncpy(configFile, inputFile, 507);
    
    // Add ".config" to the end of our confid file and see if it exists.
    strncat(configFile, ".config", 7);
    setuid(euid);
    statCheck = lstat(configFile, &statBuffer);
    fp = fopen(configFile, "r");
    setuid(ruid);

    if (statCheck == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    if (fp == NULL) {
        printf("Can't find config file %s.\n", configFile);
        exit(EXIT_FAILURE);
    }

    // Checking to make sure config file isn't a symlink
    if (S_ISLNK(statBuffer.st_mode)) {
        printf("Config file is a symlink.\n");
        exit(EXIT_FAILURE);
    }

    // Iterate through the config file and see if the user is allowed to access it.
    if (checkConfigFile(fp, getpwuid(ruid)) != 1) {
        printf("You do not have permission to copy %s.\n", inputFile);
        exit(EXIT_FAILURE);
    }

    // Set the UID for opening our desired file, then set it back immediately.
    setuid(euid);
    statCheck = lstat(inputFile, &statBuffer);
    input = fopen(inputFile, "r");
    setuid(ruid);
    output = fopen(outputFile, "w");

    if (statCheck == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    // If the file exists, create the output file and copy the contents over.
    if (input == NULL) {
        printf("Error. Could not open %s!\n", inputFile);
        exit(EXIT_FAILURE);
    } 

    // Checking to make sure input file isn't a symlink
    if (S_ISLNK(statBuffer.st_mode)) {
        printf("Input file is a symlink.\n");
        exit(EXIT_FAILURE);
    }

    if (output == NULL) {
        printf("Error. Could not create %s!\n", outputFile);
        exit(EXIT_FAILURE);
    }
    
    while ((ch = fgetc(input)) != EOF) {
        fputc(ch, output);
    }

    // Free up memory.
    fclose(fp);
    fclose(input);
    fclose(output);

    exit(EXIT_SUCCESS);
}

// This function goes through the passed in file and checks, line
// by line, for the same username as the user running the program.
// If it finds the username, it returns a 1, otherwise it returns
// a 0. Eventually this function will check the read/write permissions
// and pass back a value indicating whether the user can
// modify or just read files.
int checkConfigFile(FILE *fp, struct passwd *p)
{
    int i;
    char *user = p->pw_name;
    char line[500];
    char temp[500];

    // Iterate through each line, check up to the space where the
    // permissions are specified, and then compare the name with
    // the current user through passwd.
    while (fgets(line, sizeof(line), fp)) {
        temp[0] = '\0';
        for (i = 0; line[i] != ' '; i++) {
            temp[i] = line[i];
        }
        temp[i] = '\0';
        if (strncmp(temp, user, 500) == 0) return 1;
    }

    return 0;
}
