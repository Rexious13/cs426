# scopy
### Allows file sharing across folders on a same machine via UID permissions

In order to allow sharing for files, the person wanting to share the files must 
place scopy in a folder that the other user can access, give it setuid 
permissions (`chmod u+s scopy`), and create a config file for the file they 
wish to share.

Example for usage: if the user wishes to share "secret-file.txt", they must 
create a "secret-file.txt.config" and add the username of the user, followed by 
the permission they would like to grant the user. The permissions are `r` for 
read, `w` for write, and `b` for read/write. Config file looks like this:

`user1 r`  
`user3 w`  
`user2 b`

NOTE: The users MUST be separated by new lines, and the permissions MUST be 
separated by a space!

Finally, in order for the user to access the file, they must specify the full 
file path:  
`/home/{sourceuser}/Public/scopy  /home/{sourceuser}/Documents/secret-file.txt  
~/not-so-secret-anymore.txt`

# Compiling

Simply run `make` to compile the program, then use scopy according to the 
instructions provided.

# Security

Whenever the program has to switch over to the original user's UID, it only 
does it for as long as it needs to open the file, then immediately switches 
back to the UID of the user before continuing on. If, at any point, it 
discovers that it can't open a file for one reason or another, it immediately 
exits the program.

It also checks to make sure the config file and target file aren't symlinks
before attempting to copy anything.